require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper
  let(:product) { create(:product) }

  describe 'full title' do
    it 'have the right title in index' do
      expect(full_title('')).to eq 'BIGBAG Store'
    end

    it 'have the right title when title is nil' do
      expect(full_title(nil)).to eq 'BIGBAG Store'
    end

    it 'have the right title in show' do
      expect(full_title(product.name)).to eq("#{product.name} - BIGBAG Store")
    end
  end
end
