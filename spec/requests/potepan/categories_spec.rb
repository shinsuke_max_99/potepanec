require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :request do
  describe "GET /potepan/categories" do
    let(:taxonomy) { create(:taxonomy, name: "Categories") }
    let(:taxon1) { create(:taxon, name: "Bags", taxonomy: taxonomy) }
    let(:taxon2) { create(:taxon, name: "Mugs", taxonomy: taxonomy) }
    let!(:product_with_taxon1) { create(:product, name: "Tote bag", taxons: [taxon1]) }
    let!(:another_product_with_taxon1) { create(:product, name: "Sholder bag", taxons: [taxon1]) }
    let!(:another_product_with_taxon2) { create(:product, name: " Normal Mug", taxons: [taxon2]) }

    context "パスにtaxon1.idを使用した場合" do
      before do
        get potepan_category_path(id: taxon1.id)
      end

      it "レスポンスが正しく返ってくること" do
        expect(response).to have_http_status(200)
      end

      it 'taxonomyが表示されていること' do
        expect(response.body).to include taxonomy.name
      end

      it 'taxon1が表示されていること' do
        expect(response.body).to include taxon1.name
      end

      it 'taxon1に紐づく商品が表示されていること' do
        expect(response.body).to include product_with_taxon1.name
        expect(response.body).to include another_product_with_taxon1.name
      end

      it 'taxon2に紐づく商品が表示されていないこと' do
        expect(response.body).not_to include another_product_with_taxon2.name
      end
    end

    context "パスにtaxon2.idを使用した場合" do
      before do
        get potepan_category_path(id: taxon2.id)
      end

      it "レスポンスが正しく返ってくること" do
        expect(response).to have_http_status(200)
      end

      it 'taxon2が表示されていること' do
        expect(response.body).to include taxon2.name
      end

      it 'taxon2に紐づく商品が表示されていること' do
        expect(response.body).to include another_product_with_taxon2.name
      end

      it 'taxon1に紐づく商品が表示されていないこと' do
        expect(response.body).not_to include product_with_taxon1.name
        expect(response.body).not_to include another_product_with_taxon1.name
      end
    end
  end
end
