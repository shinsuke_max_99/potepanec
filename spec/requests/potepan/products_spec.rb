require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :request do
  describe "GET /potepan/products" do
    let(:taxon1) { create(:taxon) }
    let(:taxon2) { create(:taxon) }
    let(:product) { create(:product, name: "Tote bag", taxons: [taxon1]) }
    let!(:related_product) { create(:product, name: "Rails bag", taxons: [taxon1]) }
    let(:another_taxon_product) { create(:product, name: "Sholder bag", taxons: [taxon2]) }

    context "パスにproduct.id使用した場合" do
      before do
        get potepan_product_path(id: product.id)
      end

      it "レスポンスが正しく返ってくること" do
        expect(response).to have_http_status(200)
      end

      it 'productだけが表示されていること' do
        expect(response.body).to include product.name
        expect(response.body).not_to include another_taxon_product.name
        expect(response.body).to include related_product.name
        expect(response.body).not_to include another_taxon_product.name
      end
    end

    context "パスにanother_product.id使用した場合" do
      before do
        get potepan_product_path(id: another_taxon_product.id)
      end

      it "レスポンスが正しく返ってくること" do
        expect(response).to have_http_status(200)
      end

      it 'another_productだけが表示されていること' do
        expect(response.body).to include another_taxon_product.name
        expect(response.body).not_to include product.name
        expect(response.body).not_to include related_product.name
      end
    end
  end
end
