require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let(:taxon1)  { create(:taxon) }
  let(:taxon2)  { create(:taxon) }
  let(:product) { create(:product, name: "Tote bag", taxons: taxon) }

  describe "#related_products" do
    subject { product.related_products }

    let!(:related_product) { create(:product, name: "Mugs", taxons: [taxon1]) }

    context "関連商品が存在する時" do
      let(:taxon) { [taxon1] }

      it { is_expected.to contain_exactly(*related_product) }
    end

    context "関連商品が存在しない時" do
      let(:taxon) { [taxon2] }

      it { is_expected.to match_array([]) }
    end
  end
end
